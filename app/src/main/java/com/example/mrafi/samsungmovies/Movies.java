package com.example.mrafi.samsungmovies;

/**
 * Created by mrafi on 2/27/18.
 */

public class Movies {

    private String name;
    private String posterPath;
    private String popularity;
    private int[] genre;


    public Movies(String name, String posterPath, String popularity, int[] genre) {
       this.name=name;
       this.posterPath=posterPath;
       this.popularity=popularity;
       this.genre=genre;

    }

    String getGenree()
    {
        StringBuilder s  =new StringBuilder();
        for(int s1 : this.genre) {

            switch (s1)
            {
                case 28: s.append("Action ");
                break;
                case 12: s.append("Adventure ");
                    break;
                case 16: s.append("Animation ");
                    break;
                case 35: s.append("Comedy ");
                    break;
                case 80: s.append("Crime ");
                    break;
                case 99: s.append("Documentary ");
                    break;
                case 18: s.append("Drama ");
                    break;
                case 10751: s.append("Family ");
                    break;
                case 14: s.append("Fantasy ");
                    break;
                case 36: s.append("History ");
                    break;
                case 27: s.append("Horror ");
                    break;
                case 10402: s.append("Music ");
                    break;
                case 9648: s.append("Mystery ");
                    break;
                case 10749: s.append("Romance ");
                    break;
                case 878: s.append("Science Fiction ");
                    break;
                case 10770: s.append("TV Movie ");
                    break;
                case 53: s.append("Thriller ");
                    break;
                case 10752: s.append("War ");
                    break;
                case 37: s.append("Western ");
                    break;
                    default:
                        s.append("");
            }
        }
        return s.toString();
    }

    String getName()
    {
        return name;
    }

    String getPosterPath()
    {
        return posterPath;
    }

    String getPopularity()
    {
        return popularity;
    }

}
