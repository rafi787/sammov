package com.example.mrafi.samsungmovies;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrafi on 2/27/18.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.movieViewHolder> {

    private Context mContext;
    private ArrayList<Movies> list;
    public MovieAdapter(Context c, ArrayList<Movies> mov) {
        mContext = c;
        list = mov;
    }

    public void addData(List<Movies> data) {
        if (data != null) {
            list.addAll(data);
        }
        else
        {
            list.clear();
        }
        this.notifyDataSetChanged();
    }

    @Override
    public MovieAdapter.movieViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, viewGroup, false);
        return new movieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieAdapter.movieViewHolder holder, int position) {
        Movies mov = list.get(position);
        holder.itemView.setTag(position);
        Glide.with(mContext).load(mov.getPosterPath()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.moviePoster);
        holder.popularity.setText(mov.getPopularity());
        holder.genre.setText(mov.getGenree());
        holder.title.setText(mov.getName());

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    public class movieViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final ImageView moviePoster;
        public final TextView popularity;
        public final TextView genre;

        public movieViewHolder(View itemView) {
            super(itemView);
            moviePoster = itemView.findViewById(R.id.movie_poster);
            popularity = itemView.findViewById(R.id.popularity);
            genre = itemView.findViewById(R.id.genre);
            title = itemView.findViewById(R.id.Tittle);

        }

    }
}
