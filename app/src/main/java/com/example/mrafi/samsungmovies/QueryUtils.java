package com.example.mrafi.samsungmovies;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static com.example.mrafi.samsungmovies.MainActivity.LOG_TAG;


public final class QueryUtils {

    private static final String lang = "en-US";
    private static final String LANG = "language";
    private static final String PAGE = "page";
    private static final String BASE_URL_0 =
            "https://api.themoviedb.org/3/movie/now_playing";
    private static final String BASE_URL_1 =
            "https://api.themoviedb.org/3/movie/upcoming";



    private QueryUtils() {

    }

    public static String fetchJsonResponse(int SECTION_NUMBER, Context context) {
        URL url;
        if(SECTION_NUMBER==0) {
            url = buildUrl(context, BASE_URL_0);
        }
        else
        {
            url = buildUrl(context, BASE_URL_1);
        }
        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
        }

        return jsonResponse;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the Movie JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static URL buildUrl(Context context, String s) {
        Uri weatherQueryUri = Uri.parse(s).buildUpon()
                .appendQueryParameter("api_key", context.getString(R.string.APIKEY))
                .appendQueryParameter(LANG, lang)
                .appendQueryParameter(PAGE, "1")
                .build();

        try {
            URL QueryUrl = new URL(weatherQueryUri.toString());
            Log.v("TAG", "URL: " + QueryUrl);
            return QueryUrl;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Movies> extractFeatureFromJson(String moviesJSON) {

        // Create an empty ArrayList that we can start adding Movie to
        List<Movies> Movies = new ArrayList<>();
        try {

            JSONObject baseJasonResponse = new JSONObject(moviesJSON);
            JSONArray moviesArray = baseJasonResponse.getJSONArray("results");
            for (int i = 0; i < moviesArray.length(); i++) {

                JSONObject currentMovie = moviesArray.getJSONObject(i);
                String posterPath = currentMovie.getString("poster_path");
                String title = currentMovie.getString("original_title");
                JSONArray genre = currentMovie.getJSONArray("genre_ids");
                int[] arr = new int[genre.length()];
                for (int s = 0; s < genre.length(); s++) {
                    arr[s] = genre.getInt(s);
                }
                String popularity = currentMovie.getString("popularity");
                Movies movies = new Movies(title, getURL(posterPath), popularity, arr);
                Movies.add(movies);
            }

        } catch (JSONException e) {

            Log.e("QueryUtils", "Problem parsing the movies JSON results", e);
        }
        return Movies;
    }

    private static String getURL(String path) {
        return "https://image.tmdb.org/t/p/w342" + path.substring(0);
    }

}
