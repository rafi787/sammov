package com.example.mrafi.samsungmovies;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String LOG_TAG = MainActivity.class.getName();
    public static final String ARG_SECTION_NUMBER = "section_number";
    private static final int LOADER_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SectionsPagerAdapter mSectionsPagerAdapter;
        ViewPager mViewPager;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    public static class PlaceholderFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Movies>> {

        RecyclerView mRecyclerView;
        MovieAdapter movieAdapter;
        ProgressBar progressBar;
        TextView errorMessage;
        View view;
        LinearLayoutManager layoutManager;

        public PlaceholderFragment() {
        }
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
            progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
            errorMessage = (TextView) rootView.findViewById(R.id.empty_view);
            view = rootView.findViewById(R.id.snackbar_action);
            layoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setHasFixedSize(true);
            movieAdapter = new MovieAdapter(getActivity(), new ArrayList<Movies>());
            mRecyclerView.setAdapter(movieAdapter);
            if(checkConnectivity()) {
                getLoaderManager().initLoader(LOADER_ID, null, this);
            }
            else
            {
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(getString(R.string.ERROR));
            }
            return rootView;
        }

        private boolean checkConnectivity() {
            ConnectivityManager cm =
                    (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();

        }

        @Override
        public Loader<List<Movies>> onCreateLoader(int id, final Bundle args) {
            return new AsyncTaskLoader<List<Movies>>(getActivity()) {

                @Override
                protected void onStartLoading() {
                    errorMessage.setVisibility(View.INVISIBLE);
                    forceLoad();
                }

                @Override
                public List<Movies> loadInBackground() {
                        String jsonResponse = QueryUtils.fetchJsonResponse(getArguments().getInt(ARG_SECTION_NUMBER),getActivity());
                        return QueryUtils.extractFeatureFromJson(jsonResponse);

                }

                @Override
                public void deliverResult(List<Movies> data) {
                    super.deliverResult(data);
                }
            };
        }

        @Override
        public void onLoadFinished(Loader<List<Movies>> loader, List<Movies> data) {
            if (data != null) {
                movieAdapter.addData(data);
            }
        }

        @Override
        public void onStop() {
            super.onStop();
            movieAdapter.addData(null);

        }

        @Override
        public void onLoaderReset(Loader<List<Movies>> loader) {
            movieAdapter.addData(null);
        }

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = getString(R.string.nowplaying);
            } else if (position == 1) {
                title = getString(R.string.upcoming);
            }
            return title;
        }
    }
}
